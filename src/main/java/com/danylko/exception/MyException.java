package com.danylko.exception;

/**
 * My own Exception class.
 */
public class MyException extends Exception {
    /**
     * Constructor.
     *
     * @param message Parameter.
     */
    public MyException(String message) {
        super(message);
    }
}
