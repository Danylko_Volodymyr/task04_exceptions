package com.danylko.controller;

import com.danylko.exception.MyException;

import java.io.IOException;

/**
 * An interface to interact with Model and View.
 */
public interface Controller {
    void showProductList() throws MyException;

    void sortByPrice();

    void addProduct(String productName, int price) throws IOException;

    void saveToFile(String path);

    void downloadFromFile(String path);
}
