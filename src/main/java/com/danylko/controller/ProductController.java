package com.danylko.controller;

import com.danylko.exception.MyException;
import com.danylko.model.Model;
import com.danylko.model.Logic;


import java.io.IOException;

/**
 * This class implement Controller and used to work with Model.
 */
public class ProductController implements Controller {
    private Model products;

    /**
     * Constructor.
     */
    public ProductController() {
        this.products = new Logic();
    }

    /**
     * This method used to display list of products.
     *
     * @throws MyException MyException if list is empty.
     */
    @Override
    public void showProductList() throws MyException {
        products.showProductList();
    }

    /**
     * This method used to sort products in list by price.
     */
    @Override
    public void sortByPrice() {
        products.sortByPrice();
    }

    /**
     * This method used to add product to list.
     *
     * @param productName Product name.
     * @param price       Product price.
     * @throws IOException IOException if countProducts > 3.
     */
    @Override
    public void addProduct(String productName, int price) throws IOException {
        products.addProduct(productName, price);
    }

    /**
     * This method used to save list of product to file.
     *
     * @param path Path to save.
     */
    @Override
    public void saveToFile(String path) {
        products.saveToFile(path);
    }

    /**
     * This method used to download products from file to list.
     *
     * @param path Path to download.
     */
    @Override
    public void downloadFromFile(String path) {
        products.downloadFromFile(path);
    }
}
