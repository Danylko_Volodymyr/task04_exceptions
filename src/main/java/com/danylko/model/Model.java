package com.danylko.model;

import com.danylko.exception.MyException;

import java.io.IOException;

/**
 * An interface to interact with Controller.
 */
public interface Model {
    void showProductList() throws MyException;

    void sortByPrice();

    void addProduct(String productName, int price) throws IOException;

    void saveToFile(String path);

    void downloadFromFile(String path);
}
