package com.danylko.model;


import com.danylko.exception.MyException;

import java.io.IOException;

/**
 * This class implement Model and used to work with class ProductList.
 */
public class Logic implements Model {
    private ProductList productList;

    /**
     * Constructor.
     */
    public Logic() {
        this.productList = new ProductList();
    }

    /**
     * This method used to display list of products.
     *
     * @throws MyException MyException if list is empty.
     */
    @Override
    public void showProductList() throws MyException {
        productList.showProductList();
    }

    /**
     * This method used to sort products in list by price.
     */
    @Override
    public void sortByPrice() {
        productList.sortByPrice();
    }

    /**
     * This method used to add product to list.
     *
     * @param productName Product name.
     * @param price       Product price.
     * @throws IOException IOException if countProducts > 3.
     */
    @Override
    public void addProduct(String productName, int price) throws IOException {
        productList.addProduct(productName, price);
    }

    /**
     * This method used to save list of product to file.
     *
     * @param path Path to save.
     */
    @Override
    public void saveToFile(String path) {
        productList.saveToFile(path);
    }

    /**
     * This method used to download products from file to list.
     *
     * @param path Path to download.
     */
    @Override
    public void downloadFromFile(String path) {
        productList.downloadFromFile(path);
    }
}
