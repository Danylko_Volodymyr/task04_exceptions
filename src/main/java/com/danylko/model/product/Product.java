package com.danylko.model.product;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;

/**
 * My product class that implements interfaces AutoCloseable,
 * Comparable<Product>, Serializable.
 */
public class Product implements AutoCloseable, Comparable<Product>, Serializable {
    private String nameProduct;
    private int price;
    transient private PrintWriter file;
    private static int countProducts;

    /**
     * Constructor.
     *
     * @param nameProduct name of product.
     * @param price       price of product.
     */
    public Product(String nameProduct, int price) {
        countProducts++;
        this.nameProduct = nameProduct;
        this.price = price;
        try {
            file = new PrintWriter(this.nameProduct + ".txt");
            file.println();
            file.println("Product name: " + this.nameProduct);
            file.println("Product price: " + this.price);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to get Product name.
     *
     * @return String Product name.
     */
    public String getNameProduct() {
        return nameProduct;
    }

    /**
     * Method to get Product price.
     *
     * @return int Product price.
     */
    public int getPrice() {
        return price;
    }

    /**
     * This method used to close file, and throws IOException if countProducts > 3.
     *
     * @throws IOException IOException if countProducts > 3.
     */
    public void close() throws IOException {
        if (countProducts > 3) {
            throw new IOException("Exception method close()");
        }
        if (file != null) {
            file.close();
        }
    }

    /**
     * This method used to display product.
     */
    public void displayProduct() {
        System.out.printf("Product name: %s%nProduct price: %d%n%n",
                getNameProduct(), getPrice());
    }

    /**
     * This Override method used to compare products by price.
     * If the method returns a negative number, then the current object
     * will be placed before the one that is passed through the parameter.
     * If the method returns a positive number, then, conversely, after the second object.
     * If the method returns zero, then both objects are equal.
     *
     * @param o Compared object.
     * @return int -1 or 0 or 1.
     */
    @Override
    public int compareTo(Product o) {
        return this.price - o.price;
    }
}
