package com.danylko.model;

import com.danylko.exception.MyException;
import com.danylko.model.product.Product;

import java.io.*;
import java.util.*;

/**
 * This class used to build list of products and work with list.
 */
public class ProductList {
    private List<Product> products;

    /**
     * Constructor without parameters.
     */
    public ProductList() {
        this.products = new ArrayList<>();
    }

    /**
     * Constructor without parameters.
     *
     * @param products list of products.
     */
    public ProductList(List<Product> products) {
        this.products = products;
    }

    /**
     * This method used to add product to list.
     *
     * @param productName Product name.
     * @param price       Product price.
     * @throws IOException IOException if countProducts > 3.
     */
    public void addProduct(String productName, int price) throws IOException {
        try (Product tmp = new Product(productName, price)) {
            products.add(tmp);
        }
    }

    /**
     * This method used to sort products in list by price.
     */
    public void sortByPrice() {
        Collections.sort(products);
    }

    /**
     * This method used to display list of products.
     *
     * @throws MyException MyExeption if list is empty.
     */
    public void showProductList() throws MyException {
        if (products.isEmpty()) {
            throw new MyException("Exception!!!Product list is empty");
        }

        for (Product item : products) {
            item.displayProduct();
        }
    }

    /**
     * This method used to save list of product to file.
     *
     * @param path Path to save.
     */
    public void saveToFile(String path) {
        try (ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(path))) {
            oos.writeObject(products);
            System.out.println("File has been written");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * This method used to download products from file to list.
     *
     * @param path Path to download.
     */
    public void downloadFromFile(String path) {
        try (ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(path))) {
            products = (ArrayList<Product>) ois.readObject();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

}
