package com.danylko;

import com.danylko.view.MyView;

/**
 *<h1>Task04_exception</h1>
 *The program illustrates the exception work.
 *
 * @author Volodymyr Danylko
 * @version 1.0
 * @since 2019-11-17
 */
public class Application {
    /**
     * My method "main".
     * @param args args parameter.
     */
    public static void main(String[] args) {
        /**
         * Start of program.
         */
        new MyView().show();

    }
}
