package com.danylko.view;

import com.danylko.controller.ProductController;
import com.danylko.exception.MyException;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * This class create some UI to work.
 */
public class MyView {
    private ProductController productController;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    /**
     * Constructor
     */

    public MyView() {
        productController = new ProductController();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print Product List");
        menu.put("2", "  2 - sort by price");
        menu.put("3", "  3 - add product");
        menu.put("4", "  4 - save list to file");
        menu.put("5", "  5 - download list from file");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    /**
     * This method used to display list of products.
     *
     * @throws MyException MyException if list is empty.
     */

    private void pressButton1() throws MyException {
        productController.showProductList();
    }

    /**
     * This method used to sort products in list by price.
     */
    private void pressButton2() {
        productController.sortByPrice();
    }

    /**
     * This method used to add product to list.
     */
    private void pressButton3() {
        try {
            System.out.print("Please input product name:");
            String productName = input.nextLine();
            System.out.print("Please input product price:");
            int price = Integer.parseInt(input.nextLine());
            productController.addProduct(productName, price);
        } catch (NumberFormatException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method used to save list of product to file.
     */
    private void pressButton4() {
        System.out.print("Please input file path:");
        try {
            String path = input.nextLine();
            productController.saveToFile(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method used to download products from file to list.
     */
    private void pressButton5() {
        System.out.print("Please input file path:");
        try {
            String path = input.nextLine();
            productController.downloadFromFile(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method used to output menu.
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     *This method used to start work.
     */
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } while (!keyMenu.equals("Q"));
    }


}
