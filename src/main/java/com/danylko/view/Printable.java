package com.danylko.view;

import com.danylko.exception.MyException;

import java.io.IOException;

/**
 * Interface .
 */
public interface Printable {
    /**
     * Method print().
     *
     * @throws MyException MyExeption.
     * @throws IOException IOException.
     */
    void print() throws MyException, IOException;
}
